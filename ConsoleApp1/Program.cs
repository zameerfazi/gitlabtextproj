﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;


namespace ConsoleApp1
{
    using System;
    using System.Text.RegularExpressions;

    public class Username
    {
        public static bool Validate(string username)
        {
            var pat = new Regex(@"^[a-zA-Z0-9-]");
            if (username.Length < 6 || username.Length > 16)
            {
                return false;
            }
            else if ((username.Contains(" ")) || ((username.Split('-').Length > 2)))
            {
                return false;
            }
            else if (!char.IsLetter(username[0]))
            {
                return true;
            }
            else
            {
                return false;
            }
            Log.Fatal("Error", "An Exception occurred while Refreshing Inventory records.");
        }
        public static void Main(string[] args)
        {
            Console.WriteLine(Validate("Mike-Standish")); // Valid username
            Console.WriteLine(Validate("Mike Standish")); // Invalid username
        }
    }

}
